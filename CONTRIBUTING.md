## Contributing to Mask

While Mask is technically part of the Pandaemonium group resources, it is generally maintained and worked on by the original author [@Moderocky](https://gitlab.com/Moderocky).

I welcome PRs and other contributions. These will generally take one of three forms, and instructions can be found below.

___

#### 1. Feature Additions

Mask welcomes new features. Almost all feature-adding pull-requests will be accepted, as long as they follow the following principles:

1. Features should not compile any additional libraries. Unless absolutely necessary, please work with what is already available or, if required, copy over any GPL-covered code that is necessary. Due to the modular build structure, compiling in additional libraries is discouraged.
If necessary, a library can be added with a `provided` scope, and is then up to the user to compile.

2. Features should be in self-contained sub-packages under `com.moderocky.mask`

    |Type|Location|
    |----|--------|
    |Internal|`com.moderocky.mask.internal...`|
    |Single-class Tool|`com.moderocky.mask.api...`|
    |Multi-class Feature|`com.moderocky.mask.api.%feature-name%...`|

3. Features should either be documented, or should have a wiki entry. If they are absolutely clear, then documentation can be minimal.

4. Features should respect the dependencies of their category. Is it a Bukkit-specific feature? Put it in `Mask-Bukkit`. Is it a platform-independent feature? Perhaps try a different module.

___

#### 2. Small Improvements

Small improvements should be made without (if possible) changing the erasure of classes. This preserves upgrade compatibility for projects using Mask. If a class erasure has to be changed, the PR will probably be reserved for the next minor version update.

---

#### 3. Bug Fixes

Small bug fixes should be made without changing method erasure, if possible. This is to prevent breaking changes (see above.)


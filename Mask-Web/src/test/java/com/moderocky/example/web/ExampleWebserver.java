package com.moderocky.example.web;

import com.moderocky.mask.api.web.WebServer;
import com.moderocky.mask.api.web.event.GetEvent;

import java.net.Socket;

public class ExampleWebserver extends WebServer<ExampleWebConnection> {
    /*
    First we extend the default server, but we tell it we're using our own custom WebConnection class.
     */

    // This constructor will be used by us to create the server.
    // It doesn't have to be extended, but it makes sense to put our listeners here too.
    public ExampleWebserver(int port, String webContentRoot) {
        super(port, webContentRoot);

        registerListener(GetEvent.class, new ExampleGetListener()); // Registers a listener for get events.
    }

    @Override
    public ExampleWebConnection createConnection(Socket socket) { // This allows you to spawn web connections of your EXTENDED type
        return new ExampleWebConnection(socket, this);
    }

}

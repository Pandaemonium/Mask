package com.moderocky.example.web;

import com.moderocky.mask.api.web.WebRequestListener;
import com.moderocky.mask.api.web.event.GetEvent;

public class ExampleGetListener implements WebRequestListener<GetEvent> {
    // This is an example listener.

    @Override
    public void onEvent(GetEvent event) { // This method will be called whenever a GetEvent is triggered.
        System.out.println(event.getFileRequested());
    }

}

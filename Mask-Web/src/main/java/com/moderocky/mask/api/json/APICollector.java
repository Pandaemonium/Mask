package com.moderocky.mask.api.json;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class APICollector {

    public abstract String getID();

    public abstract Class<?> getType();

    protected abstract @NotNull List<JSonProperty> getProperties(String key);

}

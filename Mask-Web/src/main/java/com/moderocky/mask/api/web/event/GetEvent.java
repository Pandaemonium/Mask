package com.moderocky.mask.api.web.event;

import com.moderocky.mask.api.web.Method;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GetEvent extends ResponseEvent {

    public GetEvent(@NotNull WebServer<?> server, @NotNull WebConnection connection, @NotNull Socket socket, BufferedOutputStream outputStream, BufferedReader reader, PrintWriter writer, String requested) {
        super(server, connection, socket, Method.GET, outputStream, reader, writer, requested);
    }

}

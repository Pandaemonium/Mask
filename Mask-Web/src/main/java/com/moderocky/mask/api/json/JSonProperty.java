package com.moderocky.mask.api.json;

public interface JSonProperty {

    String getName();

    Object getValue();

}

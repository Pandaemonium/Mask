package com.moderocky.example.reflection;

import dev.moderocky.mirror.FieldMirror;
import dev.moderocky.mirror.MethodMirror;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class AccessReflectionExample {

    public void thingyReflectionTest() {
        Mirror<Player> player = new Mirror<>(Bukkit.getOnlinePlayers().iterator().next());
        FieldMirror<Long> fp = player.field("firstPlayed");
        if (fp.get() > 100) player.invoke("updatePlayerListHeaderFooter");
        int hash = player.<Integer>field("hash").get();
        player.ifHasField("hasPlayedBefore", field -> field.set(false));
        for (MethodMirror<Object> method : player.getMethodMirrors()) {
            boolean boo = method.tryFunc((m) -> method.isFinal(), false);
        }
    }

}

package com.moderocky.example.command;

import com.moderocky.mask.command.ArgInteger;
import com.moderocky.mask.command.ArgPlayer;
import com.moderocky.mask.command.Commander;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class MyFirstCommand extends Commander<CommandSender> {

    @Override
    public @NotNull CommandImpl create() {
        return command("my_command")
                .arg("hello", sender -> sender.sendMessage("hi!"))
                .arg("integer",
                        sender -> sender.sendMessage("Please provide an integer!"),
                        arg(
                                (sender, input) -> {
                                },
                                new ArgInteger()
                        )
                )
                .arg("kill",
                        arg(
                                (sender, input) -> ((Player) input[0]).remove(),
                                new ArgPlayer()
                        )
                );
    }

    @Override
    public @NotNull CommandSingleAction<CommandSender> getDefault() {
        return sender -> sender.sendMessage("Yay! You did /" + getCommand());
    }

}
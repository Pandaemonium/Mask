package com.moderocky.example.command;

import com.moderocky.example.MainBukkitPluginClass;
import com.moderocky.mask.command.Commander;
import com.moderocky.mask.gui.ItemFactory;
import com.moderocky.mask.gui.VisualGUI;
import com.moderocky.mask.template.WrappedCommand;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VisualGUITestCommand extends Commander<Player> implements WrappedCommand {

    @Override
    public @NotNull CommandImpl create() {
        return command("vis");
    }

    @Override
    public @NotNull CommandSingleAction<Player> getDefault() { // This provides a help message with clickable options for ALL possible arguments
        VisualGUI gui = new VisualGUI(MainBukkitPluginClass.getInstance(), 36, "test vis")
                .setLayout(new String[]{
                        "#########",
                        "#00000O0#",
                        "#0X00000#",
                        "#########"
                })
                .createTile('#', new ItemStack(Material.IRON_BARS))
                .createTile('0', new ItemStack(Material.BLACK_STAINED_GLASS_PANE))
                .createButton('O',
                        new ItemFactory(Material.SNOWBALL)
                                .addConsumer(meta -> meta.setDisplayName("snowy"))
                                .create(),
                        (player, event) -> player.sendMessage("hi! you clicked a snowball")
                )
                .createButton('X',
                        new ItemFactory(Material.TOTEM_OF_UNDYING)
                                .addConsumer(meta -> meta.setDisplayName("totem"))
                                .create(),
                        (player, event) -> player.sendMessage("hi! you clicked a totem")
                );
        return gui::open;
    }

    @Override
    public @Nullable String getPermission() {
        return "test.command.permission";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("gui_vis", "vis_test");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "Description here";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) { // Not needed - we're using the parent.
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) {
            return execute((Player) sender, args);
        }
        return false;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        List<String> strings = getPossibleArguments(String.join(" ", args));
        if (strings == null || strings.isEmpty()) return null;
        final List<String> completions = new ArrayList<>();
        StringUtil.copyPartialMatches(args[args.length - 1], strings, completions);
        Collections.sort(completions);
        return completions;
    }

}

package com.moderocky.mask.internal.utility;

import com.moderocky.mask.Mask;
import com.moderocky.mask.annotation.Internal;
import com.moderocky.mask.api.Compressive;
import com.moderocky.mask.template.BukkitPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

/**
 * A few frequently-used file methods.
 */
@Internal
public class BukkitFileManager extends ConfigFileManager implements Compressive {

    public static final ConfigFileManager MANAGER = new ConfigFileManager();

    static {
        MaskMetrics.attemptStartup();
    }

    public static FileConfiguration getFile(@NotNull String folder, @NotNull String name) {
        File path = new File(folder, name);
        return YamlConfiguration.loadConfiguration(path);
    }

    public static void putIfAbsent(@NotNull File file) {
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                //
            }
        }
    }

    public static void save(@NotNull FileConfiguration file, @NotNull File path) {
        try {
            putIfAbsent(path);
            file.save(path);
        } catch (IOException e) {
            if (Mask.getPlugin().isBukkit() && Mask.getPlugin() instanceof BukkitPlugin) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        save(file, path);
                    }
                }.runTaskLater((BukkitPlugin) Mask.getPlugin(), 30);
            }
        }
    }

}

package com.moderocky.mask.template;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import com.moderocky.mask.Mask;
import com.moderocky.mask.annotation.API;
import com.moderocky.mask.annotation.Internal;
import com.moderocky.mask.annotation.Unsafe;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.command.BukkitCommand;
import com.moderocky.mask.gui.MenuGUI;
import com.moderocky.mask.internal.utility.MaskMetrics;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * This is a template plugin class.
 * You can extend it instead of {@link JavaPlugin} in your plugins.
 * <p>
 * A few useful methods are provided within this, though mainly registration bits and bobs.
 * <p>
 * {@link #startup()} and {@link #disable()} are used instead of onEnable and onDisable.
 */
@API
@SuppressWarnings("unused")
public abstract class BukkitPlugin extends JavaPlugin implements IPlugin {

    private static BukkitPlugin instance;
    private final MaskMetrics maskMetrics = MaskMetrics.attemptStartup(this);
    private SkriptAddon addon;
    private CommandMap commandMap = null; // Bukkit, in its infinite wisdom, decided to remove the accessor in 1.16 :(

    public BukkitPlugin() {
        super();
    }

    protected BukkitPlugin(@NotNull final JavaPluginLoader loader, @NotNull final PluginDescriptionFile description, @NotNull final File dataFolder, @NotNull final File file) {
        super(loader, description, dataFolder, file);
    }

    @API
    @SuppressWarnings("unchecked")
    public static <T extends BukkitPlugin> T getInstance() {
        return (T) instance;
    }

    @Unsafe
    @Internal
    protected void setInstance(@Nullable BukkitPlugin plugin) {
        instance = plugin;
    }

    /**
     * Calls a Bukkit event
     *
     * @param event The event
     * @param <Z> The type
     */
    @API
    public static <Z extends Event> void callEvent(Z event) {
        Bukkit.getScheduler().runTask(getInstance(), () -> Bukkit.getServer().getPluginManager().callEvent(event));
    }

    /**
     * Returns a metadata value for the object attached to your plugin.
     *
     * @param object The object
     * @return The metadata value
     */
    @API
    public static FixedMetadataValue getMetaValue(Object object) {
        return new FixedMetadataValue(getInstance(), object);
    }

    /**
     * Generates a {@link NamespacedKey} for your plugin
     *
     * @param string The key
     * @return The namespaced key.
     */
    @API
    public static NamespacedKey getNamespacedKey(String string) {
        return new NamespacedKey(getInstance(), string);
    }

    /**
     * @return True if this server is running paper, otherwise false
     */
    public static boolean isPaper() {
        try {
            Class.forName("co.aikar.timings.Timing");
            return true;
        } catch (final ClassNotFoundException e) {
            return false;
        }
    }

    /**
     * @return Your plugin version from the plugin.yml
     */
    @API
    public String getVersion() {
        return getDescription().getVersion();
    }

    /**
     * @return Your plugin API version from the plugin.yml
     */
    @API
    public String getAPIVersion() {
        return getDescription().getAPIVersion();
    }

    /**
     * @return Your plugin description from the plugin.yml
     */
    @API
    public String getPluginDescription() {
        return getDescription().getDescription();
    }

    /**
     * @return Your plugin's full name from the plugin.yml
     */
    @API
    public String getFullName() {
        return getDescription().getFullName();
    }

    /**
     * @return Your plugin's prefix from the plugin.yml
     */
    @API
    public String getPrefix() {
        return getDescription().getPrefix();
    }

    /**
     * @return Whether all the soft dependencies of your plugin are met
     */
    @API
    public boolean areDependenciesMet() {
        if (getDescription().getSoftDepend().size() > 0)
            for (String string : getDescription().getSoftDepend()) {
                if (Bukkit.getPluginManager().getPlugin(string) == null)
                    return false;
            }
        return true;
    }

    /**
     * @return The craftbukkit version of the server, with a format like 'v1_15_R1'
     */
    @API
    public String getCraftVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    }

    @API
    public void disablePlugin() {
        Bukkit.getPluginManager().disablePlugin(this);
    }

    /**
     * This is called on enable.
     * <p>
     * It performs some internal operations, then calls the {@link #startup()} method.
     */
    @Override
    @Internal
    public void onEnable() {
        setInstance(this);
        registerMask();
        registerAddon();
        startup();
        registerManagers();
        registerProtocol();
        registerListeners();
        registerRecipes();
        registerEvents();
        registerCommands();
        if (Mask.getMask().hasSkript()) {
            registerSyntax();
        }
    }

    /**
     * This is called on disable.
     * <p>
     * It performs some internal operations, then calls the {@link #disable()} method.
     */
    @Override
    @Internal
    public void onDisable() {
        disable();
        setInstance(null);
        unregisterMask();
    }

    /**
     * @return This plugin as a Skript addon (iff Skript is present)
     */
    @API
    public SkriptAddon getAddon() {
        return addon;
    }

    @Internal
    private void unregisterMask() {
        Mask.setMask(null, null);
    }

    @Internal
    private void registerMask() {
        Mask.setMask(new Mask(), this);
        //Mask.getMask().updateAddonConfig();
    }

    @Internal
    private void registerAddon() {
        if (Mask.getMask().hasSkript()) {
            addon = Skript.registerAddon(this);
        } else {
            addon = null;
        }
    }

    /**
     * @return The plugin's "config" folder location
     */
    @API
    public String getConfigPath() {
        return "plugins/" + getName() + "/config.yml";
//        return getDataFolder().toString() + File.pathSeparator + "config.yml";
//        return "plugins" + File.pathSeparator + getName() + File.pathSeparator + "config.yml";
    }

    /**
     * Stub
     */
    @API
    public void resetConfig() {

    }

    /**
     * Register annotation-generated Bukkit commands.
     *
     * @param commands The commands to register
     */
    protected void register(BukkitCommand... commands) {
        for (BukkitCommand command : commands) {
            command.register(this);
        }
    }

    /**
     * This registers multiple {@link WrappedCommand}s at once.
     * <p>
     * Wrapped commands are able to be configured entirely in their own class, and do not
     * need to be added to your plugin.yml file.
     * <p>
     * They are registered to Bukkit's command map as regular plugin commands.
     *
     * @param commands Your wrapped commands
     */
    @API
    protected void register(WrappedCommand... commands) {
        for (WrappedCommand command : commands) {
            try {
                Constructor<PluginCommand> commandConstructor = PluginCommand.class.getDeclaredConstructor(String.class, org.bukkit.plugin.Plugin.class);
                if (!commandConstructor.isAccessible())
                    commandConstructor.setAccessible(true);
                PluginCommand pluginCommand = commandConstructor.newInstance(command.getCommand(), getInstance());
                pluginCommand.setAliases(command.getAliases());
                pluginCommand.setDescription(command.getDescription());
                pluginCommand.setPermission(command.getPermission());
                pluginCommand.setPermissionMessage(command.getPermissionMessage());
                pluginCommand.setUsage(command.getUsage());
                pluginCommand.register(getCommandMap());
                if (getCommandMap().register(command.getCommand(), getName(), pluginCommand)) {
                    pluginCommand.setExecutor(command);
                    pluginCommand.setTabCompleter(command);
                } else {
                    Command com = getCommandMap().getCommand(pluginCommand.getName());
                    if (com instanceof PluginCommand) {
                        ((PluginCommand) com).setExecutor(command);
                        ((PluginCommand) com).setTabCompleter(command);
                    }
                    Bukkit.getLogger().log(Level.WARNING, "A command '/" + command.getCommand() + "' is already defined!");
                    Bukkit.getLogger().log(Level.WARNING, "As this cannot be replaced, the executor will be overridden.");
                    Bukkit.getLogger().log(Level.WARNING, "To avoid this warning, please do not add WrappedCommands to your plugin.yml.");
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean isRegistered(WrappedCommand command) {
        return (getCommand(command.getCommand()) != null);
    }

    protected boolean isRegistered(String command) {
        return (getCommand(command) != null);
    }

    /**
     * This registers your menu templates.
     * You can also just register them as listeners using {@link #register(org.bukkit.event.Listener...)}.
     *
     * @param guis Menus
     */
    @API
    protected void register(MenuGUI... guis) {
        for (MenuGUI gui : guis) {
            Bukkit.getServer().getPluginManager().registerEvents(gui, getInstance());
        }
    }

    /**
     * This registers your listeners.
     *
     * @param listeners Listeners
     */
    @API
    protected void register(org.bukkit.event.Listener... listeners) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        if (listeners == null) return;
        for (org.bukkit.event.Listener listener : listeners) {
            manager.registerEvents(listener, getInstance());
        }
    }

    /**
     * Used to register Mask Listeners, which are based on functional interfaces.
     *
     * @param listeners Register fast, functional, consumer-based listeners.
     * @return The Bukkit listeners, if required.
     */
    @API
    protected org.bukkit.event.Listener[] register(Listener<?>... listeners) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        MagicList<org.bukkit.event.Listener> list = new MagicList<>();
        for (Listener<?> listener : listeners) {
            list.add(register(listener));
        }
        return list.toArray();
    }

    /**
     * Used to register Mask Listeners, which are based on functional interfaces.
     *
     * @param listeners Register fast, functional, consumer-based listeners.
     * @param lifetime The lifetime of the listener in ticks, until it is unregistered.
     * @return The Bukkit listeners, if required.
     */
    @API
    protected org.bukkit.event.Listener[] register(long lifetime, Listener<?>... listeners) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        Bukkit.getScheduler().runTaskLater(this, () -> Arrays.asList(listeners).forEach(HandlerList::unregisterAll), lifetime);
        return listeners;
    }

    @API
    protected org.bukkit.event.Listener register(Listener<?> listener) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        manager.registerEvents(listener, getInstance());
        return listener;
    }

    @API
    protected org.bukkit.event.Listener register(Listener<?> listener, long lifetime) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        org.bukkit.event.Listener thing = createListener(listener);
        manager.registerEvents(thing, getInstance());
        Bukkit.getScheduler().runTaskLater(this, () -> HandlerList.unregisterAll(thing), lifetime);
        return thing;
    }

    private <T extends Event> ClosedListener<T> createListener(Listener<T> listener) {
        return (listener::event);
    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerRecipes() {

    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerListeners() {

    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerCommands() {

    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerManagers() {
    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerProtocol() {
    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerSyntax() {
    }

    /**
     * This is called during enable. It exists purely as a structural thing, if needed.
     */
    @API
    protected void registerEvents() {
    }

    protected final CommandMap getCommandMap() {
        if (commandMap == null)
            commandMap = new Mirror<>(Bukkit.getServer()).<CommandMap>field("commandMap").get();
        return commandMap;
    }

    /**
     * Stub class to be used anonymously by consumer listeners.
     */
    @FunctionalInterface
    public interface ClosedListener<T extends Event> extends org.bukkit.event.Listener {
        @EventHandler
        void onEvent(T event);
    }

}

package com.moderocky.mask.api.chat;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * The string reader from MagicLogic.
 * Copied here so that extra dependencies are not required!
 * <p>
 * This is a rotary char-array reader.
 * It is designed to function similar to the native byte-divider (rotate - check - rotate - check...)
 * <p>
 * This is designed to be super-small and super-fast.
 */
public final class StringReader implements Iterable<Character> {

    final char[] chars;
    transient int position;

    public StringReader(String string) {
        chars = string.toCharArray();
    }

    public String readRest() {
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            builder.append(chars[position]);
            position++;
        }
        return builder.toString();
    }

    public String read(int length) {
        int end = position + length;
        StringBuilder builder = new StringBuilder();
        while (position < end) {
            if (position >= chars.length) break;
            builder.append(chars[position]);
            position++;
        }
        return builder.toString();
    }

    public String readUntil(char c) {
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            char test = chars[position];
            if (c == test) break;
            builder.append(test);
            position++;
        }
        return builder.toString();
    }

    public String readUntilMatches(Function<String, Boolean> function) {
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            char test = chars[position];
            builder.append(test);
            position++;
            if (function.apply(builder.toString())) break;
        }
        return builder.toString();
    }

    public String readUntilMatches(Pattern pattern) {
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            char test = chars[position];
            builder.append(test);
            if (pattern.matcher(builder.toString()).matches()) break;
            position++;
        }
        return builder.toString();
    }

    public String readUntilMatchesAfter(Pattern pattern, char end) {
        StringBuilder builder = new StringBuilder();
        boolean canEnd = false;
        while (canRead()) {
            char test = chars[position];
            if (test == end) canEnd = true;
            if (canEnd && pattern.matcher(builder.toString()).matches()) break;
            builder.append(test);
            position++;
        }
        return builder.toString();
    }

    public boolean hasNext() {
        return position < chars.length - 1;
    }

    public void skip() {
        if (canRead()) position++;
    }

    public void skip(int i) {
        position += i;
    }

    public void rotateBack(int i) {
        position -= i;
    }

    public boolean canRead() {
        return position < chars.length && position >= 0;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int i) {
        position = i;
    }

    public int length() {
        return chars.length;
    }

    public char current() {
        if (canRead())
            return chars[position];
        throw new RuntimeException("Limit exceeded!");
    }

    public char next() {
        if (position + 1 < chars.length)
            return chars[position + 1];
        throw new RuntimeException("Limit exceeded!");
    }

    public char rotate() {
        if (canRead()) {
            char c = chars[position];
            position++;
            return c;
        } else throw new RuntimeException("Limit exceeded!");
    }

    public void reset() {
        position = 0;
    }

    @Override
    public String toString() {
        return new String(chars);
    }

    @Override
    public Iterator<Character> iterator() {
        return new Itr();
    }

    @Override
    @SuppressWarnings("all")
    public StringReader clone() {
        StringReader reader = new StringReader(new String(chars));
        reader.position = position;
        return reader;
    }

    private class Itr implements Iterator<Character> {
        int cursor;       // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such
        int size = chars.length;

        Itr() {
        }

        public boolean hasNext() {
            return cursor != chars.length;
        }

        public Character next() {
            checkForComodification();
            int i = cursor;
            if (i >= chars.length)
                throw new NoSuchElementException();
            cursor = i + 1;
            return chars[lastRet = i];
        }

        public void remove() {
            throw new ConcurrentModificationException();
        }

        @Override
        @SuppressWarnings("unchecked")
        public void forEachRemaining(Consumer<? super Character> consumer) {
            Objects.requireNonNull(consumer);
            final int size = chars.length;
            int i = cursor;
            if (i >= size) {
                return;
            }
            while (i != size) {
                consumer.accept(chars[i++]);
            }
            // update once at end of iteration to reduce heap write traffic
            cursor = i;
            lastRet = i - 1;
            checkForComodification();
        }

        final void checkForComodification() {
        }
    }

}

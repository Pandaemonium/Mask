package com.moderocky.mask.api.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Tag implements ITag { // Contains built-in tags as an enum.

    TRANSLATE {
        final Pattern pattern = Pattern.compile("(<trans(?:late)?='([\\w\\W]+)'>|<trans(?:late)?='([\\w\\W]+)',with=\\['([\\w\\W]+)'\\]>)");
        final Pattern simple = Pattern.compile("<trans(?:late)?='([\\w\\W]+)'>");
        final Pattern multi = Pattern.compile("<trans(?:late)?='([\\w\\W]+)',with=\\['([\\w\\W]+)'\\]>");

        @Override
        public boolean matches(String string) {
            return pattern.matcher(string).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            String key;
            String[] with;
            Matcher matcher;
            if ((matcher = multi.matcher(string)).find()) {
                key = matcher.group(1);
                with = matcher.group(2).split("', ?'");
            } else {
                (matcher = simple.matcher(string)).matches();
                key = matcher.group(1);
                with = new String[0];
            }
            return new TranslatableComponent(key, (Object[]) with);
        }
    },
    SELECTOR {
        @Override
        public boolean matches(String string) {
            return (string.startsWith("<sel='") || string.startsWith("<selector='")) && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            return new SelectorComponent(chopOut(string));
        }
    },
    SCORE {
        final Pattern pattern = Pattern.compile("<score='([\\w\\W]+)',obj(?>ective)?='([\\w\\W]+)'>");
        Matcher matcher;

        @Override
        public boolean matches(String string) {
            return (matcher = pattern.matcher(string)).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            String score = matcher.group(1);
            String objective = matcher.group(2);
            return new ScoreComponent(score, objective);
        }
    },
    KEY {
        final Map<String, KeybindComponent> keys = new HashMap<String, KeybindComponent>() {{
            for (Field field : Keybinds.class.getFields()) {
                try {
                    keys.put(field.getName(), new KeybindComponent(field.get(Keybinds.class).toString()));
                } catch (Throwable ignore) {} // Future versions might have non-keybind fields, who knows?
            }
        }};

        @Override
        public boolean matches(String string) {
            return (string.startsWith("<key='") || string.startsWith("<keybind='") || string.startsWith("<key_bind='"))
                    && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            String key = chopOut(string);
            return keys.getOrDefault(key, new KeybindComponent(key));
        }
    },
    SHOW {
        final Pattern pattern = Pattern.compile("<(?>show(?>_text)?|hover)='([\\w\\W]+)'>");
        Matcher matcher;

        @Override
        public boolean matches(String string) {
            return (matcher = pattern.matcher(string)).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(matcher.group(1))));
            return component;
        }
    },
    ITEM { // LEGACY item style - takes an NBT string
        final Pattern pattern = Pattern.compile("<(?>show_)?item='([\\w\\W]+)'>");
        Matcher matcher;

        @Override
        public boolean matches(String string) {
            return (matcher = pattern.matcher(string)).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, TextComponent.fromLegacyText(matcher.group(1))));
            return component;
        }
    },
    ENTITY { // LEGACY entity style - takes an NBT string
        final Pattern pattern = Pattern.compile("<(?>(?>show_)?entity|ent)='([\\w\\W]+)'>");
        Matcher matcher;

        @Override
        public boolean matches(String string) {
            return (matcher = pattern.matcher(string)).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ENTITY, TextComponent.fromLegacyText(matcher.group(1))));
            return component;
        }
    },
    SUGGEST {
        final Pattern pattern = Pattern.compile("<(?>suggest|sgt)(?>_cmd|_command)?='([\\w\\W]+)'>");
        Matcher matcher;

        @Override
        public boolean matches(String string) {
            return (matcher = pattern.matcher(string)).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, matcher.group(1)));
            return component;
        }
    },
    COPY {
        @Override
        public boolean matches(String string) {
            return (string.startsWith("<copy='") || string.startsWith("<copy_to_clipboard='")) && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, chopOut(string)));
            return component;
        }
    },
    RUN {
        final Pattern pattern = Pattern.compile("<(?>run(?>_cmd|_command)?|cmd|command)='([\\w\\W]+)'>");
        Matcher matcher;

        @Override
        public boolean matches(String string) {
            return (matcher = pattern.matcher(string)).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, matcher.group(1)));
            return component;
        }
    },
    URL {
        @Override
        public boolean matches(String string) { // We can avoid regex with this! :)
            return (string.startsWith("<url='") || string.startsWith("<open_url='")) && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, chopOut(string)));
            return component;
        }
    },
    FILE { // This one is technically blocked by Minecraft I think
        @Override
        public boolean matches(String string) { // We can avoid regex with this! :)
            return (string.startsWith("<file='") || string.startsWith("<open_file='")) && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, chopOut(string)));
            return component;
        }
    },
    PAGE {
        @Override
        public boolean matches(String string) { // We can avoid regex with this! :)
            return (string.startsWith("<page='") || string.startsWith("<change_page='")) && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.CHANGE_PAGE, chopOut(string)));
            return component;
        }
    },
    FONT {
        @Override
        public boolean matches(String string) {
            return string.startsWith("<font='") && string.endsWith("'>");
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.setFont(chopOut(string));
            return component;
        }
    },
    RESET {
        final Pattern PATTERN = Pattern.compile("<reset.*>");

        @Override
        public boolean matches(String string) {
            return PATTERN.matcher(string).matches();
        }

        @Override
        public BaseComponent convert(String string) {
            TextComponent component = new TextComponent("");
            component.retain(ComponentBuilder.FormatRetention.NONE);
            return component;
        }

        @Override
        public void work(ComponentBuilder builder, String string) {
            if (string.contains("format"))
                builder.append("").retain(ComponentBuilder.FormatRetention.EVENTS);
            else if (string.contains("col"))
                builder.append("").retain(ComponentBuilder.FormatRetention.FORMATTING);
            else builder.append("").retain(ComponentBuilder.FormatRetention.NONE);
        }
    },
    NEW_LINE {
        @Override
        public boolean matches(String string) {
            return string.equalsIgnoreCase("<nl>")
                    || string.equalsIgnoreCase("<newline>")
                    || string.equalsIgnoreCase("<new_line>");
        }

        @Override
        public BaseComponent convert(String string) {
            return new TextComponent("\n");
        }
    };

    private static String chopOut(String string) { // Used to avoid regex grouping in simple matches
        StringReader reader = new StringReader(string);
        reader.readUntil('\''); // Reads out the "<change_page="
        reader.skip(); // Skips the '
        String segment = reader.readRest(); // Reads out the rest
        return segment.substring(0, segment.length()-2); // Cuts off the ending '>
    }

}

package com.moderocky.mask.config;

import java.io.File;

public interface ConjunctionPart<T> {

    ConjunctionPart<T> getParent();

    T getRawObject();

    File getFile();

}

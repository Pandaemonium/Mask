package com.moderocky.mask.config;

import java.io.File;

public interface MemorySection<T> extends Section, ConjunctionPart<T> {

    @Override
    MemorySection<T> getParent();
}

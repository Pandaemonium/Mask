package com.moderocky.mask.config;

public interface Section {

    String getCurrentPath();

    <T extends Section> T getSection(String path);

    default boolean hasSection(String path) {
        return exists(path) && isSection(path);
    }

    boolean isSection(String path);

    boolean isValue(String path);

    boolean isSet(String path);

    boolean exists(String path);

}

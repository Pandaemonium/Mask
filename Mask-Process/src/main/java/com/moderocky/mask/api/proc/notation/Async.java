package com.moderocky.mask.api.proc.notation;

import java.lang.annotation.*;

@Target({ElementType.PACKAGE,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Async {
}

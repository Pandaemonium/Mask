package com.moderocky.example.config;

import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.template.Config;
import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleConfig implements Config {

    @Configurable.Comment({
            "This is line 1.", "This is line 2! :D"
    })
    public boolean enabled = true;
//    this will be 'enabled: true' in the file

    public boolean allowBlockBreaking = true;
//    the fieldname is converted to snake_case
//    this will show up in the file as...
//    world_settings:
//        allow_block_breaking: true

    @Configurable.Bounded(minValue = 0, maxValue = 16)
    public int someValue = 4;
//    If a user inputs a value outside the bounds it will be rounded inside the bounds.
//    Works for all number types.

    public boolean overriddenValue = true;
//    This will ALWAYS be reset to true.
//    Basically a static value in the config, for a version number, or something.

    @Configurable.Keyed("the_node_name")
    public boolean test = false;
//    Instead of using the field name (snake-cased) this will use the specified node name.

    @Configurable.Regex(matcher = ".+", alternative = "default_value")
    public String yourString = "hi";
//    if the regex fails, the alternative is used instead

    public Material material = Material.STONE;
//    This will be automatically serialised to "STONE" for the config, and vice-versa.
//    Should with any enum

    public List<String> strings = new MagicList<String>() {{
        add("Hello!");
        add("this is line two :o");
        add(":))) :D :o :P");
    }};
//    Any String-type list should be serialisable.
//    Other collections may be, but this is untested! :o
//    As seen here, you can use Mask's "Magic List"! :)

    public Map<String, String> sectionMapHere = new HashMap<String, String>() {{
        put("test", "testing!");
        put("a", "b?");
        put("another", "test...");
    }};
//    Your map will be serialised to key: value sub-entries
//    The key MUST be a string
//    The value must be a string or number type
//    Enums are NOT supported, I can't reliably obtain the field generic to convert them back :(
//    This can be quite unstable

    public ExampleConfig() {
        load();
    }
//    Just a constructor, you could load it separately.
//    Makes life simpler, though.

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/MyPluginFolder/";
    }
//    The folder where you want this to go.

    @Override
    public @NotNull String getFileName() {
        return "test_config.yml";
    }
//    The file-name.

}

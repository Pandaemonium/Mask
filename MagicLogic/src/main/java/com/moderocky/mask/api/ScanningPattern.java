package com.moderocky.mask.api;

public class ScanningPattern {

    protected final String pattern;
    protected final MagicList<Part> order = new MagicList<>();
    protected MagicStringList catchment;

    public ScanningPattern(String string) {
        pattern = string;
        StringProcessor processor = new StringProcessor(string);
        int i = 0;
        while (processor.canRead()) {
            String s = processor.readOutUntil("$g");
            if (processor.hasApproaching(1)) {
                if (!s.isEmpty()) {
                    order.add(new Static(s));
                    i++;
                }
                order.add(new Catcher(i));
                processor.skip(2);
            } else {
                String end = s + processor.readRest();
                order.add(new Static(end));
            }
            i++;
        }
    }

    protected ScanningPattern(ScanningPattern pattern) {
        this.pattern = pattern.pattern;
        this.order.addAll(pattern.order);
        this.catchment = pattern.catchment;
    }

    public synchronized String[] runWith(StringProcessor processor) {
        catchment = new MagicStringList();
        for (Part part : order) {
            part.run(processor);
        }
        return catchment.toArray();
    }

    @Override
    @SuppressWarnings("all")
    public ScanningPattern clone() {
        return new ScanningPattern(this);
    }

    protected static class Static implements Part {
        final CharSequence string;

        public Static(CharSequence string) {
            this.string = string;
        }

        @Override
        public void run(StringProcessor reader) {
            reader.readOut(string);
        }
    }

    protected class Catcher implements Part {
        final int index;
        public Catcher(int index) {
            this.index = index+1;
        }

        @Override
        public void run(StringProcessor reader) {
            if (order.size() > index) {
                Part part = order.get(index);
                if (part instanceof Static)
                    catchment.add(reader.readOutUntil(((Static) part).string));
            }
            else catchment.add(reader.readRest());
        }
    }

    protected interface Part {
        void run(StringProcessor reader);
    }

}

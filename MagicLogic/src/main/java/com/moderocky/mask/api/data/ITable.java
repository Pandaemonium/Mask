package com.moderocky.mask.api.data;

import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;

public interface ITable<P> {

    Gson GSON = new Gson();

    static @Nullable String readContent(File file) {
        if (file == null || !file.exists() || !file.isDirectory()) return null;
        try (FileInputStream stream = new FileInputStream(file);
             Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
             BufferedReader input = new BufferedReader(reader)) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = input.readLine()) != null) {
                builder.append(line);
                builder.append(System.lineSeparator());
            }
            return builder.toString().isEmpty() ? null : builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    static void writeContent(@NotNull File file, String string) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.print(string != null ? string : "");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    static <Q> Q[] newArray(Class<Q> qClass, int length) {
        return (Q[]) Array.newInstance(qClass, length);
    }

    @SuppressWarnings("unchecked")
    static <Q, R> Q newArray(Class<R> cls, int... dimensions) {
        return (Q) Array.newInstance(cls, dimensions);
    }

    @SuppressWarnings("unchecked")
    static <Q, R> Q newMultiplicativeArray(Class<R> cls, int... dimensions) {
        int length = 1;
        for (int dimension : dimensions) {
            if (dimension > 0)
                length = dimension * length;
        }
        return (Q) Array.newInstance(cls, length);
    }

    P[] asLinearRepresentation();

}

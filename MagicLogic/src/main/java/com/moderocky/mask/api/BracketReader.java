package com.moderocky.mask.api;

public class BracketReader extends StringProcessor {

    public BracketReader(String string) {
        this(string.toCharArray());
    }

    public BracketReader(char[] chars) {
        super(chars);
    }

    public BracketReader(StringReader reader) {
        super(reader.chars);
        this.position = reader.position;
    }

    public String readBracketPair(char open, char close) {
        int depth = 0;
        boolean ignore = false;
        super.readUntil(open);
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            char current = rotate();
            builder.append(current);
            if (ignore) ignore = false;
            else if (current == '\\') ignore = true;
            else if (current == close) {
                depth--;
                if (depth < 1) break;
            } else if (current == open) depth++;
        }
        return builder.toString();
    }

    public boolean hasBracketPair(char open, char close) {
        int depth = 0;
        boolean has = false;
        boolean ignore = false;
        super.readUntil(open);
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            char current = rotate();
            builder.append(current);
            if (ignore) ignore = false;
            else if (current == '\\') ignore = true;
            else if (current == close) {
                depth--;
                if (depth < 1) break;
            } else if (current == open) {
                depth++;
                has = true;
            }
        }
        return has && depth == 0;
    }

    public boolean hasText(char delimiter) {
        final char[] chars = this.remaining();
        int i = 0;
        for (char c : chars) {
            if (c == delimiter) i++;
        }
        return i > 1;
    }

    public boolean hasTextApproaching(char delimiter) {
        BracketReader reader = this.clone();
        if (!reader.hasText(delimiter)) return false;
        reader.skipWhitespace();
        return reader.current() == delimiter;
    }

    public String readText(char delimiter) {
        boolean ignore = false;
        StringBuilder builder = new StringBuilder();
        readUntil(delimiter);
        while (canRead()) {
            char current = rotate();
            if (ignore) ignore = false;
            else if (current == '\\') ignore = true;
            else if (current == delimiter) break;
            else {
                builder.append(current);
            }
        }
        return builder.toString();
    }

    public String readUntil(char c, char next) {
        StringBuilder builder = new StringBuilder();
        while (canRead()) {
            char test = this.chars[position];
            if (c == test && hasNext() && next() == next) break;
            builder.append(test);
            position++;
        }
        return builder.toString();
    }

    @Override
    @SuppressWarnings("all")
    public BracketReader clone() {
        BracketReader reader = new BracketReader(chars);
        reader.position = position;
        return reader;
    }

}

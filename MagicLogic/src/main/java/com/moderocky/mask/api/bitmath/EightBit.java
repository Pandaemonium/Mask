package com.moderocky.mask.api.bitmath;

public class EightBit {

    private byte bi;

    public EightBit(byte by) {
        bi = by;
    }

    public void set(int index, boolean boo) {
        if (index > 7) throw new IllegalArgumentException("This has only eight bits.");
        if (boo) bi |= 1 << index;
        else bi &= ~(1 << index);
    }

    public boolean get(int index) {
        if (index > 7) throw new IllegalArgumentException("This has only eight bits.");
        return (bi >>> index & 1) == 1;
    }

    public byte getByte() {
        return bi;
    }

}

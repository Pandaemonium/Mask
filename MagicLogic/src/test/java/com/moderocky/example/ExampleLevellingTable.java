package com.moderocky.example;

import com.moderocky.mask.api.data.FixedNDimensionalTable;

public class ExampleLevellingTable extends FixedNDimensionalTable<Integer> {

    public static final ExampleLevellingTable TABLE = new ExampleLevellingTable(int.class, 100, 2, 3);

    public static final int COMBAT = 0;
    public static final int MELEE = 0;
    public static final int RANGED = 1;
    public static final int MAGIC = 2;

    public static final int FARMING = 1;
    public static final int COWS = 0;
    public static final int PIGS = 1;
    public static final int SHEEP = 2;

    public ExampleLevellingTable(Class<Integer> cls, int... dimensions) {
        super(cls, dimensions);
    }

    protected ExampleLevellingTable(FixedNDimensionalTable<Integer> table) {
        super(table);
    }


}
